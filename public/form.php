<!-- <!DOCTYPE html>
<html lang="ru">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="style.css" !important>

<body>
    <h2>Форма</h2>
    <form action="" method="POST">
        <label>
            Имя: <br />
            <input name="names" value=" " />
        </label><br />

        <label>
            Email:<br />
            <input name="e_mail" value=" " type="email" />
        </label><br />

        <label>
            Дата рождения:<br />
          
            <select name="hb">
              <option value="1990">1990</option>
              <option value="1991">1991</option>
              <option value="1992">1992</option>
              <option value="1993">1993</option>
              <option value="1994">1994</option>
              <option value="1995">1995</option>
            </select>
        </label><br />

        Пол:<br />
        <label><input type="radio" checked="checked" name="gender" value="Значение1" />
            Женский</label>
        <label><input type="radio" name="gend" value="Значение2" />
            Мужской</label><br />

        Количество конечностей:<br />
        <label><input type="radio" checked="checked" name="limbs" value="Значение1" />
            1</label>
        <label><input type="radio" name="limbs" value="Значение2" />
            2</label>
        <label><input type="radio" name="limbs" value="Значение3" />
            3</label>
        <label><input type="radio" name="limbs" value="Значение4" />
            4</label><br />

        <label>
            Сверхспособности:
            <br />
            <select name="field-name-1[]" multiple="multiple">
                <option value="Значение1">бессмертие</option>
                <option value="Значение2" selected="selected">прохождение сквозь стены</option>
                <option value="Значение3" selected="selected"> левитация</option>
            </select>
        </label><br />

        <label>
            Биография:<br />
            <textarea name="bio"></textarea>
        </label><br />

        <label><input type="checkbox" checked="checked" name="check" />
            С контрактом ознакомлен </label><br />

        <input type="submit" value="Отправить" />
    </form>
</body>

</html> -->
<!DOCTYPE html>
<html lang="ru">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="style.css" !important>

<body>
<div id="sun">
    <h2>Форма</h2>
    <form action="" method="POST">
        <label>
            Имя: <br />
            <input name="fio" value=" " />
        </label><br />

        <label>
            Email:<br />
            <input name="email" value=" " type="email" />
        </label><br />

             <label>
            Дата рождения:<br />
          
            <select name="birthday">
              <option value="1990">1990</option>
              <option value="1991">1991</option>
              <option value="1992">1992</option>
              <option value="1993">1993</option>
              <option value="1994">1994</option>
              <option value="1995">1995</option>
            </select><br />

        Пол:<br />
        <label><input type="radio" checked="checked" name="gender" value="Значение1" />
            Женский</label>
        <label><input type="radio" name="gender" value="Значение2" />
            Мужской</label><br />

        Количество конечностей:<br />
        <label><input type="radio" checked="checked" name="limbs" value="Значение1" />
            1</label>
        <label><input type="radio" name="limbs" value="Значение2" />
            2</label>
        <label><input type="radio" name="limbs" value="Значение3" />
            3</label>
        <label><input type="radio" name="limbs" value="Значение4" />
            4</label><br />

        <label>
            Сверхспособности:
            <br />
            <select name="abilities[]" multiple="multiple">
                <option value="1" selected="selected">бессмертие</option>
                <option value="2" selected="selected">прохождение сквозь стены</option>
                <option value="3" selected="selected"> левитация</option>
            </select>
        </label><br />

        <label>
            Биография:<br />
            <textarea name="biography"></textarea>
        </label><br />

        <label><input type="checkbox" checked="checked" name="check" />
            С контрактом ознакомлен </label><br />

        <input type="submit" value="Отправить" />
    </form>
</div>
</body>

</html>